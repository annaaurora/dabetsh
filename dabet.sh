#! /bin/sh
if [[ $1 = "--help" ]]
then
	VERSION="0.1.0"

	echo "dabet"
	echo "Print the duration between two times in ISO8601 format"
	echo
	echo "USAGE:"
	echo "	dabet [OPTIONS]"
	echo
	echo "OPTIONS:"
	echo "	-f, --duration-format <DURATION_FORMAT>	Format of the duration. If this option is not set the default format will be used [possible values: rfc3339, iso8601]"
	echo "	-h, --help				Print help information"
	echo "	-V, --version				Print version information"
else
	# Use arguments
	STARTTIME_ISO8601="$1"
	ENDTIME_ISO8601="$2"

	# Set Unix times
	STARTTIME_UNIX=$(date -d "$STARTTIME_ISO8601" +%s --utc)
	ENDTIME_UNIX=$(date -d "$ENDTIME_ISO8601" +%s --utc)

	# Set durations
	DURATION_UNIX=$(($ENDTIME_UNIX-$STARTTIME_UNIX))
	DURATION_DAYS=$(($DURATION_UNIX/60/60/24))

	# Print duration in days
	echo $DURATION_DAYS
fi
